from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("flux_villoteau_hatte.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class Utilisateur(BaseModel, UserMixin):
    login = CharField(unique=True)
    passwd = CharField()

class Flux(BaseModel):
    userID = BigIntegerField()
    url = CharField()

def create_tables():
    with database:
        database.create_tables([Utilisateur,Flux ])
        print("Creation tables")

def drop_tables():
    with database:
        database.drop_tables([Utilisateur,Flux ])
        print("Drop tables")