from flask import Flask , abort, render_template, redirect, url_for, request
from models import Utilisateur, create_tables, drop_tables, Flux
from forms import Inscription, Connexion, Ajout
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import click, os, hashlib, feedparser

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = os.urandom(16).hex()

login_manager = LoginManager() 
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return Utilisateur.get(id=user_id)

"""----------------------- route ---------------------"""

@app.route('/')
def home():
    return render_template('main.html')

@app.route('/inscription', methods=['GET', 'POST', ])
def inscription():
     
    form = Inscription()

    if form.validate_on_submit():
        login = form.login.data
        passwd = form.passwd.data
        passwd = hashlib.sha1(passwd.encode()).hexdigest()
        user = Utilisateur.select().where(Utilisateur.login == login).first()
        if(user == None):
            utilisateur = Utilisateur()
            utilisateur.login = login
            utilisateur.passwd = passwd
            utilisateur.save()
            return redirect(url_for('login'))
        else:
            return redirect(url_for('inscription'))
    return render_template('inscription.html', form = form)

@app.route('/login', methods=['GET', 'POST', ])
def login():
    
    form = Connexion()

    if form.validate_on_submit():
        login = form.login.data
        password = hashlib.sha1(form.passwd.data.encode()).hexdigest()
        user = Utilisateur.select().where((Utilisateur.login == login) & (Utilisateur.passwd == password)).first()
        if (user != None):
            login_user(user)
            current_user.idUtilisateur = user.id
            return redirect(url_for('feed'))

    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/feed', methods=['GET', 'POST', ])
@login_required
def feed():
    listeFluxParse = []
    listeFluxUser = Flux.select().where(Flux.userID == current_user.id)
    for item in listeFluxUser :
        fluxparse = feedparser.parse(item.url) 
        listeFluxParse.append(fluxparse)
    return render_template('feed.html',listeFluxParse=listeFluxParse,listeFluxUser=listeFluxUser,zip=zip(listeFluxParse,listeFluxUser))

@app.route('/ajout', methods=['GET', 'POST', ])
@login_required
def ajout():
    form = Ajout()
    if form.validate_on_submit():
        select = Flux.select().where((Flux.url == form.url.data) & (Flux.userID == current_user.id)).first()
        if(select == None):
            flux = Flux()
            flux.userID = current_user.id
            flux.url = form.url.data
            flux.save()
            return redirect(url_for('feed'))   
        else:
            return redirect(url_for('ajout'))
    return render_template('addFlux.html',form=form)

@app.route('/supp', methods=['GET','POST'])
@login_required
def suppFlux():
    idFluxUser = request.args.get('userID')
    linkFlux = request.args.get('url')
    requete = Flux.delete().where((Flux.userID == idFluxUser) & (Flux.url == linkFlux))
    requete.execute()
    return redirect(url_for('feed'))

@app.route('/flux', methods=['GET', 'POST'])
@login_required
def affich():
    linkFlux = request.args.get('url')
    fluxparse = feedparser.parse(linkFlux)
    element = feedparser.parse(linkFlux).entries
    return render_template('viewFlux.html',fluxparse=fluxparse, element=element)

@app.route('/ok')
def ok():
    return render_template('ok.html')

"""------------------------commande-------------------------"""

@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')


@app.cli.command()
def requete():
    for test in Utilisateur.select():
        click.echo(test.login)
    click.echo('fin recherche')

@app.cli.command()
def truc():
    utilisateur = Utilisateur()
    utilisateur.login = "test"
    password = "123"
    utilisateur.passwd = hashlib.sha1(password.encode()).hexdigest()
    utilisateur.save()
    click.echo(' test   123')