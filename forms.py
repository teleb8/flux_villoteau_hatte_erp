from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired
from wtforms.widgets import PasswordInput
from feedparser import *

class Inscription(FlaskForm):
    login = StringField('Login', validators=[DataRequired(),])
    passwd = StringField('Mot de passe', validators=[DataRequired(),], widget=PasswordInput(hide_value=True))
    pass 

class Connexion(FlaskForm):
    login = StringField('Login', validators=[DataRequired(),])
    passwd = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class Ajout(FlaskForm):
    url = StringField('Flux ', validators=[DataRequired()])
    pass