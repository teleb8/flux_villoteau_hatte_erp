# Flux_Villoteau_Hatte_ERP

## Installation

```sh
 $ pipenv install
```


## Execution

```sh
 $ pipenv shell
 (pipenv-shell)$ flask run
```


### Initialisation de la base de donnée
la base est vierge
```sh
 (pipenv-shell)$ flask initdb
```


# Fonctionnement
la page d'acceuil nous permet de nous inscrire et de nous connecter. une fois connecter on peut voir le noms des flux ou nous somme inscrit. si on clique sur c'est nom on peut voir les derniers flux de cela et en cliquant sur chacun on arrive a la page du site qui la produit.

on peut rajouter des flux depuis la barre de navigation situer en haut.
